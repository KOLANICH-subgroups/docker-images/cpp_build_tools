Scripts for building an image containing tools needed for building C++ programs
===============================================================================

Scripts in this repo are [Unlicensed ![](https://raw.githubusercontent.com/unlicense/unlicense.org/master/static/favicon.png)](https://unlicense.org/).

Third-party components have own licenses.

**DISCLAIMER: BADGES BELOW DO NOT REFLECT THE STATE OF THE DEPENDENCIES IN THE CONTAINER**

This builds a Docker image contains the following components:
  * [`registry.gitlab.com/kolanich-subgroups/docker-images/fixed_python:latest` Docker image](https://gitlab.com/KOLANICH-subgroups/docker-images/fixed_python), each component of which has own license;
  * `vanilla CMake`
  * `ninja` build system
  * `meson` build system
  * `ccache`
  * `premake5`
  * `ckati`
  * dependencies of everything above.
