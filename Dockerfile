# syntax=docker/dockerfile:experimental
FROM registry.gitlab.com/kolanich-subgroups/docker-images/prebuilder:latest
LABEL maintainer="KOLANICH"
WORKDIR /tmp
ADD ./*.sh ./
ADD ./pythonPackagesToInstallFromGit.txt ./
ADD ./add_repo.py ./
RUN \
  set -ex;\
  python3 ./add_repo.py "vanilla_CMake_KOLANICH" "https://kolanich-subgroups.gitlab.io/packages/build-tools/CMake_deb" "CFE0968467EB727FF3E19F276CDE8B298420AF04";\
  python3 ./add_repo.py "build_tools_KOLANICH" "https://kolanich-subgroups.gitlab.io/packages/build-tools/cpp_build_tools" "F7245DAA5F3C4ADF9C30435220486A680275B5E5";\
  apt-get update;\
  apt-get install -y ccache vanilla-cmake ninja-build;\
  apt-get install -y libcli11-dev;\
  apt-get install -y kati bake premake5;\
  ./installPythonPackagesFromGit.sh ;\
  apt-get autoremove --purge -y ;\
  apt-get clean && rm -rf /var/lib/apt/lists/* ;\
  rm ./add_repo.py;\
  rm -r ~/.cache/pip || true ;\
  rm -rf /tmp/*
